# sast-playground

```
$ git clone https://gitlab.com/jdsalaro-group/gitlab-hackathon-jdsalaro-sast-playground

...

$ cd gitlab-hackathon-jdsalaro-sast-playground

...

$ pipenv install

...

$ pipenv shell

...

$ semgrep scan -c rule.yml target
```
