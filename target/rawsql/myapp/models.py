from django.db import models


class Person(models.Model):
    name = models.CharField("name", max_length=200)
    birthdate = models.DateTimeField("birthdate")

    def __str__(self):
        return f"{self.name}({self.birthdate})"
